%% Programming Example 6.4.1 - TPDF Dither
% DigitalAudioTheory.com

d_TPDF = zeros(size(d));
d_TPDF(1) = d(1);	% the first value of d_tri is the same as d

% each subsequent sample is a difference with the preceding value
d_TPDF(2:end) = d(2:end)-d(1:end-1);


x8d_TPDF = quantBits(x+d_TPDF,N,A);
plot(t, x8d_TPDF);
hold on; box on;
plot(t, x)


figure();
% for Matlab use:
histogram(d_TPDF, linspace(-q, q, 50), 'Normalization', 'pdf')
% for Octave use:
hist(d_TPDF, linspace(-q, q, 50));
xlim([-q q])


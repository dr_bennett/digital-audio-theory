%% Programming Example 2.4.1 - Beat Frequencies
% DigitalAudioTheory.com

fs=10000;
t=0:1/fs:1;             % time vector
x1=cos(2*pi*100*t);     % tone 1 (100 hz)

soundsc(x1 + cos(2*pi*101*t), fs); %1 hz delta
soundsc(x1 + cos(2*pi*102*t), fs); %2 hz delta
soundsc(x1 + cos(2*pi*103*t), fs); %3 hz delta

x2=cos(2*pi*104*t);     % tone 2 (104 hz)
soundsc(x1+x2, fs);
plot(t, x1+x2)          % combination
 
% plotting a tone at 102 Hz with a beat frequency of 2 Hz
beat=2*cos(2*pi*2*t);
x3=cos(2*pi*102*t).*beat;
 
hold on;
plot(t, x3)
plot(t, beat, 'LineWidth', 4)

%% Programming Example 2.1.3 - Complex Vectors
% DigitalAudioTheory.com

theta = pi/6;       % angle
r = 1/2;            % magnitude
x=r*exp(j*theta);   % complex vector
 
% draw the tip of the vector
polarplot(theta, r, 's', 'MarkerSize', 20)

title('x=0.5e^{j\pi/6}')
pax = gca;
pax.ThetaAxisUnits = 'radians';   % Matlab only
rlim([0 1])                       % Matlab only
 
% or use compass, which plots a vector on the complex plane
compass(x);


% get projection onto real axis Re{x}
a = r * cos(theta)
 
% get projection onto imaginary axis Im{x}
b = r * sin(theta)
 
% use native commands to get the same result
real(x)
imag(x)
 
% double check with pythagorous
Mag = sqrt( a^2 + b^2 )
Ph = atan(b/a)

% native commands
abs(x)
angle(x)


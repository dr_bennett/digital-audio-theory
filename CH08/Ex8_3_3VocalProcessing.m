%% Programming Example 8.3.3 - Vocal Processing
% DigitalAudioTheory.com

% Setup the audio recorder
Fs=44100;
nbits=16;
nchans=1;
r = audiorecorder(Fs, nbits, nchans);

% Record 2 sec of audio
recordblocking(r, 2); 
% for Matlab use:
x = r.getaudiodata;
% for Octave use:
x = getaudiodata(r);
x = sum(x,2);

% Audition the recording
play(r);
 
% Load the ir
h = audioread('IR.wav');
 
% Prepare Output
len = length(x)+length(h)-1;
y=zeros(len,1);
 
tic
% for every possible shift amount
for n=0:len-1 
    % for every k from 0 to n; except before complete overlap 
    % when we can stop at the length of h (the length of the filter)
    for k=0:min(length(h)-1,n)
	  % check to see if h has shifted beyond the bounds of x 
        if ((n-k)+1 < length(x))
		% +1 in every array argument since Matlab sequences start
		% at index 1 (and not 0, like other programming languages)
            y(n+1) = y(n+1)+ h(k+1)*x((n-k)+1); 
        end
    end
end
t=toc

% Audition the output
soundsc(y, Fs)
 
% Built-In Command for convolution
tic
y2=conv(x,h);
t=toc

soundsc(y2, Fs)

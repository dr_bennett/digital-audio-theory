%% Programming Example 8.4.1 - Time Delay Estimation
% DigitalAudioTheory.com

x=audioread('drum.wav');
w=audioread('drum_delay.wav');
 
 
[similarity,lag] = xcorr(w,x);
[~,I] = max(abs(similarity));
 
plot(lag,similarity)
title(sprintf('Delay of %f sec',lag(I)/44100))
 



nx = length(x);
ny = length(y);

% Initialize the cross-correlation vector
cross_corr = zeros(1, nx + ny - 1);

% Perform cross-correlation
for m = 1:nx + ny - 1
    for n = 1:nx
        if (n + m - ny > 0) && (n + m - ny <= nx)
            cross_corr(m) = cross_corr(m) + x(n) * y(n + m - ny);
        end
    end
end

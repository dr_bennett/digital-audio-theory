%% Programming Example 10.2.1 - First Order IIR Filter
% DigitalAudioTheory.com

% 1st order IIR filter magnitude
a0=0.9;
a1=0.9;
b1=-0.1;
fs=48000; 
 
% method 1
f=0:1:fs/2;     % freq array
H1 = (a0*exp(j*2*pi*f/fs)+a1)./(exp(j*2*pi*f/fs)+b1);
plot(f,db(abs(H1)));
grid on; xlabel('freq'); ylabel('gain (dB)')
 
%% method 2
num=[a0, a1];
den=[1, b1];  

[H1,F]=freqz(num,den, fs, fs);
subplot(3,1,1)
semilogx(F,db(abs(H1))); grid on; xlim([20 20000])
subplot(3,1,2)
semilogx(F, angle(H1)); grid on; xlim([20 20000])
 
% and group delay
[Grd,~]=grpdelay(num,den, fs, fs);
subplot(3,1,3); 
semilogx(F, Grd); grid on; xlim([20 20000])
 
% p/z plot
zplane(num,den)


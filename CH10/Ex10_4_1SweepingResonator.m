%% Programming Example 10.4.1 - Sweeping Resonator
% DigitalAudioTheory.com

T   = 2; 	% duration (sec)
fs  = 44100;
f0  = 100; 	% starting freq (Hz)
fBW = 100;  % bandwidth (Hz)
 
B  = fBW/fs*2*pi;  % bandwidth (normalized)
A  = 1-B/2;	       % pole radius
 
ynm1=0;
ynm2=0;
 
for n=1:T*fs
    % generate a sample of white noise   
    x=2*(rand(1,1)-0.5); 
    
    % sweep f0 by 0.1 Hz every sample
    f0 = f0+0.1; 
    w0 = f0/fs*2*pi;
    
    % determine true pole location for peak resonance at f0
    wx=acos(cos(w0)*2*A/(1+A^2)); 
    
    % normalization factor (gain of x[n])
    a0=(1-A^2)*sin(w0); 
    
    % implement resonator difference equation
    y(n)=a0*x+2*A*cos(wx)*ynm1-A^2*ynm2; 
    
    ynm2=ynm1;  % store ynm1 into ynm2 
    ynm1=y(n);  % store current output into ynm1
    
end
 
sound(y, fs)

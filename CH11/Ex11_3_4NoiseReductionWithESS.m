%% Programming Example 11.3.4 - Noise Reduction With ESS
% DigitalAudioTheory.com

fa = 20;
fb = 20000;
T = 20;
sil = 0.1;

n_rms = [];
for fs = [44100, 48000, 96000, 192000]

    % ESS
    ESS = sweeptone(T,sil,fs,'SweepFrequencyRange',[fa fb]);
    
    % simulated DUT
    [DUT_a, DUT_b] = butter(2,5000/(fs/2));
    
    % Drive DUT twice, add random noise
     y_1 = filter(DUT_a, DUT_b, ESS+0.1*randn(size(ESS)));
     y_2 = filter(DUT_a, DUT_b, ESS+0.1*randn(size(ESS)));
    
    % Extract IR
    ir_1=impzest(ESS, y_1);
    ir_2=impzest(ESS, y_2);
    
    % normalize
    ir_1 = ir_1/max(abs(ir_1));
    ir_2 = ir_2/max(abs(ir_2));
    
    % LPF IRs at fb
    [LPF_a, LPF_b]=butter(2,fb/(fs/2));
    
    ir_1f = filter(LPF_a, LPF_b, ir_1);
    ir_2f = filter(LPF_a, LPF_b, ir_2);
    
    % plot IRs
    hold on;
    plot((0:50)/fs, (ir_1f(1:51)+ir_2f(1:51))/2);
    
    % estimate noise levels
    n = ir_1f-ir_2f;
    n_rms = [n_rms rms(n)];
    
    % db difference of noise levels compared to 44.1 kHz
    if (length(n_rms)>1)
        db(n_rms(1)/n_rms(end))
    end

end

% inverse sweep level vs. freq
semilogx(f, db(fa/L*exp(-log(f/fa)))); grid on

% inverse sweep level vs. time
plot(L*log(f/fa), db(fa/L*exp(-log(f/fa))))


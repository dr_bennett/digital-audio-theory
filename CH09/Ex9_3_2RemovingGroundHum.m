%% Programming Example 9.3.2 - Removing Ground Hum
% DigitalAudioTheory.com

ak=1;
k=200;
Hmag=sqrt(1+2*ak*cos(2*pi*f*k/Fs)+ak^2);
plot(f, 20*log10(Hmag));
box on; grid on; 
ylabel('Gain (dB)'); 
xlabel('Freq (Hz)'); 
title('Ground Hum Cancellation')
axis([f(1) f(end) -30 10])


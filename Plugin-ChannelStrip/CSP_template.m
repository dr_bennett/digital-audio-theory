classdef CSP_template < audioPlugin
    % downloaded from: bennettaudio.com
    
    properties
        % a property is a public variable that usually has a parameter
        % associated with it

        % PARAMETER_NAME = some value;
        
    end

    properties (Constant)
        % this contains instructions to build your plugin GUI, usually
        % populated with audioPluginParamters that link to properties
        
        % parameters have:
        % a function call - audioPluginParameter('PARAMETER_NAME',...
        %'DisplayName'
        %'Label'
        %'Mapping': 'lin', 'log', 'pow', 'int', 'enum'

        
    end

    properties (Access = private)
        %internal filter variables, such as coefficient values
        FS = 44100;
        
    end
    
    methods
        function plugin = CSP_Template()
            % Do any initializing that needs to occur BEFORE the plugin runs


        end

        function out = process(plugin,in)
            % DSP section
            
            out = in;
            
        end
        
        function reset(plugin)
            % this gets called if the sample rate changes or if the plugin
            % gets reloaded
            
            plugin.FS = getSampleRate(plugin);

            
        end
        
       
        
    end
end